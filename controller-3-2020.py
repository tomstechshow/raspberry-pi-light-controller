# Light Controller
# By: Thomas Nelson
# Ver: 3.005
#
# Fix lights not staying on after song
# Updating Timing Code
# Rewrite Sync code for control
# Convert to python3

import time
import csv
import datetime
from pygame import mixer
from pylibftdi import BitBangDevice
import smtplib
from email.message import EmailMessage
import random

mixer.init()
time1 = time.localtime()
action_triggered = False
relay1 = BitBangDevice(device_id='A8008uu0')
ch_white = 1
ch_blue = 2
ch_green = 4
ch_red = 8
ch_ice = 16
ch_tree = 32
ch_net = 64
ch_stars = 128

def set_idle(hr):
    hr = time1.tm_hour
    relay1.port = 255

def play_chime():
    action_next = "Play Chime"
    chime_count = 0
    print(action_next)
    mixer.music.load("/home/pi/Documents/LightController/bell.mp3")
    if (time1.tm_hour >= 8 and time1.tm_hour <= 23):
        if (time1.tm_hour >=13):
            chime_count = time1.tm_hour-12
        if time1.tm_min > 0:
            chime_count = 1
        while chime_count > 0:
            print(chime_count)
            mixer.music.play()
            relay1.port = 128
            time.sleep(2)
            relay1.port = 0
            time.sleep(2)
            chime_count=chime_count-1
        action_triggered = False
        set_idle(time1.tm_hour)
        time.sleep(60)
    return

def play_song(track):
    x=0
    action_next = "Playing Song"
    if (track == 1):
        media1 = "/home/pi/Documents/LightController/jingle-bells.mp3"
        sequence1 = "/home/pi/Documents/LightController/jingle-bells.csv"
        medialength = 142470
        tracklength = 2900
    if (track == 2):
        media1 = "/home/pi/Documents/LightController/o-holy-night.mp3"
        sequence1 = "/home/pi/Documents/LightController/o-holy-night.csv"
        medialength = 186305
        tracklength = 3850
    if (track == 3):
        media1 = "/home/pi/Documents/LightController/we-wish-you-a-merry-christmas.mp3"
        sequence1 = "/home/pi/Documents/LightController/we-wish-you-a-merry-christmas.csv"
        medialength = 66672
        tracklength = 2661
    print(action_next)
    with open(sequence1, newline='') as f:
        r = csv.reader(f)
        l = list(r)
    l_rows = len(l)
    mixer.music.load(media1)
    mixer.music.play()
    #time.sleep(.7)
    track = .964
    while x < l_rows:
        # time.sleep(47/1000)
        track = track + .00000015
        print (track)
        x = int(((mixer.music.get_pos()/medialength)*(tracklength))*track)
        print(mixer.music.get_pos())
        print(x)
        if (x < tracklength):
            lights = int(l[x][0]) + int(l[x][1]) + int(l[x][2]) + int(l[x][3]) + int(l[x][4]) + int(l[x][5]) + int(l[x][6]) + int(l[x][7])
            relay1.port = lights
           # print (l[x][0],"|",l[x][1],"|",l[x][2],"|",l[x][3],"|",l[x][4],"|",l[x][5],"|",l[x][6],"|",l[x][7])
        if (mixer.music.get_pos() == -1 ):
            x = 999999
    action_triggered = False
    print("song done")
    set_idle(255)
    return

def main_loop():
    time.sleep(2)
    time1 = time.localtime()
    print(time1.tm_hour,":",time1.tm_min,sep="")
    #play_song(4)
    if (time1.tm_hour >= 17 and time1.tm_hour <= 23):
        set_idle(255)
        if (time1.tm_min == 0 or time1.tm_min == 15 or time1.tm_min == 30 or time1.tm_min == 45): 
            action_next = "chime"
            play_chime()
        if (time1.tm_min == 12 or time1.tm_min == 40):
            action_next = "song"
            play_song(1)
        if (time1.tm_min == 19 or time1.tm_min == 49):
            action_next = "song"
            play_song(2)
        if (time1.tm_min == 6 or time1.tm_min == 33):
            action_next = "song"
            play_song(3) 
    else:
        if (time1.tm_hour > 21 and time1.tm_hour <= 23):
            relay1.port = 1
        else: 
            relay1.port = 0   
    print(relay1.port)
    return

#notify('System Startup')
print("System Start")
set_idle(255)
while True:
  #  play_song(3)
    main_loop()
#end





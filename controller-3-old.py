# Light Controller
# By: Thomas Nelson
# Ver: 3.001
# Updating Timing Code
# Rewrite Sync code for control
# Convert to python3
# Rewrite Email Sender

import time
import csv
import datetime
from pygame import mixer
from pylibftdi import BitBangDevice
import smtplib
from email.message import EmailMessage
import random

mixer.init()
time1 = time.localtime()
action_triggered = False
relay1 = BitBangDevice(device_id='A8008uu0')  #Enter Your Device ID
ch_white = 1
ch_blue = 2
ch_green = 4
ch_red = 8
ch_ice = 16
ch_tree = 32
ch_net = 64
ch_stars = 128
to_mail = 'sent_to_email@gmail.com'
from_mail = 'send_from_eamil@gmail.com'
mail_pass = 'YourEmailPassword'

def notify(text):
    msg = EmailMessage()
    msg['Subject'] = 'Light Notify ' + str(time1.tm_hour) + '-' + str(time1.tm_min)
    msg['From'] = from_mail
    msg['To'] = to_mail
    msg.set_content(text)
    s = smtplib.SMTP('smtp.gmail.com:587')
    s.starttls()
    s.login(from_mail,mail_pass)
    s.send_message(msg)
    s.quit()


def set_idle(hr):
    if hr == 12:
        relay1.port = ch_white
    if hr == 18:
        relay1.port = int(random.random()*128)+1
    if hr == 19:
        relay1.port = int(random.random()*128)+1
    if hr == 20:
        relay1.port = ch_green
    if hr == 21:
        relay1.port = ch_blue
    if hr == 0:
        relay1.port = 0
    return

def play_chime():
    action_next = "Play Chime"
    chime_count = 0
    print(action_next)
    mixer.music.load("/home/pi/Documents/LightController/bell.mp3")
    if (time1.tm_hour >= 8 and time1.tm_hour <= 21):
        if (time1.tm_hour >=13):
            chime_count = time1.tm_hour-12
     #   notify('Chime Triggered')
        if time1.tm_min > 0:
            chime_count = 1
        while chime_count > 0:
            print(chime_count)
            mixer.music.play()
            relay1.port = 128
            time.sleep(2)
            relay1.port = 0
            time.sleep(2)
            chime_count=chime_count-1
        action_triggered = False
        set_idle(time1.tm_hour)
        time.sleep(60)
    return

def play_song(track):
    x=0
    action_next = "Playing Song"
 #   notify('Song Triggered')
    if (track == 1):
        media1 = "/home/pi/Documents/LightController/jingle-bells.mp3"
        sequence1 = "/home/pi/Documents/LightController/jingle-bells.csv"
        medialength = 142470
        tracklength = 2841
    if (track == 2):
        media1 = "/home/pi/Documents/LightController/o-holy-night.mp3"
        sequence1 = "/home/pi/Documents/LightController/o-holy-night.csv"
        medialength = 186305
        tracklength = 3700
    if (track == 3):
        media1 = "/home/pi/Documents/LightController/we-wish-you-a-merry-christmas.mp3"
        sequence1 = "/home/pi/Documents/LightController/we-wish-you-a-merry-christmas.csv"
        medialength = 66672
        tracklength = 2661
    print(action_next)
    with open(sequence1, newline='') as f:
        r = csv.reader(f)
        l = list(r)
    l_rows = len(l)
    mixer.music.load(media1)
    mixer.music.play()
    time.sleep(.8)
    while x < l_rows:
        # time.sleep(47/1000)
        x = int((mixer.music.get_pos()/medialength)*tracklength)
        # print(mixer.music.get_pos())
        if (x < tracklength):
            lights = int(l[x][0]) + int(l[x][1]) + int(l[x][2]) + int(l[x][3]) + int(l[x][4]) + int(l[x][5]) + int(l[x][6]) + int(l[x][7])
            relay1.port = lights
        #  x = x + 1
    action_triggered = False
    set_idle(time1.tm_hour)
    return

#notify('System Startup')
print("System Start")
while True:
   # print "X"
    time.sleep(2)
    time1 = time.localtime()
    if (time1.tm_hour >= 8 and time1.tm_hour <= 21):
        # print "."
        if (time1.tm_min == 0 or time1.tm_min == 15 or time1.tm_min == 30 or time1.tm_min == 45): 
            action_next = "chime"
            play_chime()
        if (time1.tm_min == 9 or time1.tm_min == 39):
            action_next = "song"
            play_song(1)
        if (time1.tm_min == 19 or time1.tm_min == 49):
            action_next = "song"
            play_song(2)

    else:
        set_idle(0)




